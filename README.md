# NEURON DEMO 

This is an example project for developing applications using Loka Neuron based on ESP-IDF v3.3.

## Getting Started

Following this tutorial you will be able to create your own aplications that include Loka Neuron libraries for using Sigfox protocol, deepsleep mode, UART, SPIFFS. You can find a complete documentation in https://neuron-sdk.lantern.rocks/.
### Prerequisites

* Install and configure ESP-IDF >= v3.3 following the steps described in https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html.

* Download mkspiffs version for the platform that you are using from https://github.com/igrr/mkspiffs/releases. **The name of the zip should start with "mkspiffs-0.2.3-esp-idf-"**. Create a folder named mkspiffs in ~esp/esp-idf and then extract the content of the downloaded zip into ~esp/esp-idf/mkspiffs folder.

* Clone this example project repository https://gitlab.com/lantern-public/neuron-demo.git where you want. 


### Compile and run the Project 

1. From terminal, change directory to cloned example project folder and then run **make menuconfig**. The console will show a menu where you need to change following configurations: 

* In serial flasher: ensure to select the correct serial port used by device when is connected (Usually /dev/ttyUSB0).

2. Run **make -j8** to compile the project and ESP-IDF components.

3. Connect the Neuron device and run **./update_info.sh** to write necessary partitions information on Neuron flash memory **Ensure that the port selected in ./update_info.sh file matches with device selected (Usually /dev/ttyUSB0)s. 

4. Then run **make flash** to flash the compiled program into the Neuron

5. Finally run **make monitor** to see the output logs of the flashed program. 

6. If you detect weird behavior in flash memory you can run **make erase_flash** for delete the content of flash memory.


## Examples 

### Sigfox Uplink

Send a message over Sigfox network in specific RCZ zone. If you are encoding with low level bits or char* you can use 
std::string(value) to parse the value to string and then send it. 

```
#include "LOKA_NEURON_S2LP.h"       // Header for S2LP Module for Sigfox Protocol

std::string rcz = "RCZ2"            // Define the RCZ zone in which message will be send 
std::string data = "test"           // Define the data to send

drivers::S2LP sigfox_driver(rcz);   // Instanciate Sigfox S2LP
sigfox_driver.turnOn();             // Turning Sigfox Radio On
sigfox_driver.sendData(data);       // Send data over Sigfox Network
sigfox_driver.turnOff();            // Turning off Sigfox Radio

```

### Sigfox Downlink

Send a message over Sigfox network in specific RCZ zone and wait for downlink message.

```
#include "LOKA_NEURON_S2LP.h"                                               // Header for S2LP Module for Sigfox Protocol

std::string rcz = "RCZ2"                                                    // Define the RCZ zone in which message will be send 
std::string data = "test"                                                   // Define the data to send

drivers::S2LP sigfox_driver(rcz);                                           // Instanciate Sigfox S2LP
sigfox_driver.turnOn();                                                     // Turning Sigfox Radio On
std::string downlink_message = sigfox_driver.sendAndReadData(data);         // Send data over Sigfox Network
sigfox_driver.turnOff();                                                    // Turning off Sigfox Radio

```


### Deep sleep Mode

Put the device in deepsleep mode that for amount of seconds consumming less than 10uA. After deepsleep the device wake up with a reset and run the main application. **This method is not secure for less than 3 seconds.**

```
#include "LOKA_NEURON_Power.h"          // Header for low power consumption

int seconds = 10;                       // Amount of seconds that device will be in deepsleep mode
drivers::NeuronPower power_driver;      // Instanciate Neuro power manager
power_driver.sleep(seconds,NULL);       // Going to deepsleep

```

## Hardware Notes

### USB Programmer Circuit

This diagram show a example of programmer circuit based on CP2104 module:

![Programmer based on CP2104](assets/prog_complete.jpg?raw=true "Programmer based on CP2104")

Only need a USB to UART TTL module (FT232RL, CH340G or CP2104) with Rx, Tx, RST and DTR pins and the following set of transistor to create signaling needed by Neuron module.
(by example: This [FT232RL](https://www.sparkfun.com/products/12731) module.)

![Autoreset circuit](assets/prog_autoreset.jpg?raw=true "Autoreset circuit")
