/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "LOKA_NEURON_Power.h"
#include "LOKA_NEURON_S2LP.h"



extern "C" {
    void app_main(void);
}


void app_main()
{
    printf("Hello world 2\n");
    unsigned char* message = (unsigned char*)"123";
    printf("Turning Sigfox Radio On\n");
    drivers::S2LP sigfox_driver;
    sigfox_driver.turnOn();
    printf("Sending Sigfox Message\n");
    sigfox_driver.sendData("TEST");
    printf("Turning Sigfox Radio Off\n");
    sigfox_driver.turnOff();
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    printf("Going to sleep now.\n");
    drivers::NeuronPower power_driver;
    power_driver.sleep(10,NULL);
}
